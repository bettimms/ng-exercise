import {Component, OnInit} from '@angular/core';
import {UserService} from '../core/services/user.service';
import {User} from '../core/models/user.model';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';

@Component({
  selector: 'ng-e-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  users: Observable<Array<User>>;
  userId = -1;//show message in case of null Id from api

  constructor(private router: Router, private userService: UserService) {
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.users = this.userService.get();
  }

  onUser(user) {
    this.userId = user.id;
    localStorage.setItem('user', JSON.stringify(user));

    // this.userService.selectedUser = user;//1st approach: Using service, in case of refresh the user is lost
    if (!user.id) {
      console.log('%c Please try a user with non-nullable Id!', 'background: #222; color: #bada55');
      return;
    } else console.clear();

    this.router.navigate(['/user-detail/', user.id]);
  }
}
