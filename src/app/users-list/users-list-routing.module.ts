import {RouterModule, Routes} from '@angular/router';
import {UsersListComponent} from './users-list.component';
import {NgModule} from '@angular/core';

export const routes: Routes = [
  {
    path: '',
    component: UsersListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class UsersListRoutingModule {

}
