import {async, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {UsersListComponent} from './users-list.component';
import {PageHeaderComponent} from '../core/components/page-header/page-header.component';
import {UserService} from '../core/services/user.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('UsersListComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        PageHeaderComponent,
        UsersListComponent
      ],
      providers: [UserService]
    }).compileComponents();
  }));
  it('should create the component', (async () => {
    const fixture = TestBed.createComponent(UsersListComponent);
    const comp = fixture.debugElement.componentInstance;
    expect(comp).toBeTruthy();
  }));
  it(`should have a table element'`, async(() => {
    const fixture = TestBed.createComponent(UsersListComponent);
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('table').classList).toContain('table');
  }));
});
