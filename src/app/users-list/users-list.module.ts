import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersListComponent} from './users-list.component';
import {UsersListRoutingModule} from './users-list-routing.module';
import {RouterModule} from '@angular/router';
import {CoreModule} from '../core/core.module';

@NgModule({
  imports: [
    CommonModule,
    UsersListRoutingModule,
    CoreModule,
    RouterModule
  ],
  declarations: [UsersListComponent]
})
export class UsersListModule {
}
