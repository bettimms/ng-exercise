import {CapitalizePipe} from './capitalize.pipe';


describe('Capitalize.PipePipe', () => {
  it('create an instance', () => {
    const pipe = new CapitalizePipe();
    expect(pipe).toBeTruthy();
  });
  it('should capitalize string', () => {
    const expectedValue = "Betim"
    const pipe = new CapitalizePipe();
    expect(pipe.transform("betim")).toEqual(expectedValue);
  });
});
