import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppHeaderComponent} from './components/app-header/app-header.component';
import {AppContentComponent} from './components/app-content/app-content.component';
import {RouterModule} from '@angular/router';
import {AppService} from './services/app.service';
import {HttpClientModule} from '@angular/common/http';
import {PageHeaderComponent} from './components/page-header/page-header.component';
import {CapitalizePipe} from './pipes/capitalize.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ],
  declarations: [AppHeaderComponent, AppContentComponent, PageHeaderComponent,CapitalizePipe],
  exports: [AppHeaderComponent, AppContentComponent, PageHeaderComponent,CapitalizePipe],
  providers: [
    AppService,
  ]
})
export class CoreModule {
}
