import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class AuthenticationService {
  private isLoggedIn = new BehaviorSubject<boolean>(false);
  loginStatus = this.isLoggedIn.asObservable();

  onLogin(status: boolean) {
    this.isLoggedIn.next(status);
  }
}
