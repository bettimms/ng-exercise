import {inject, TestBed} from '@angular/core/testing';
import {UserService} from './user.service';
import {HttpClientModule} from '@angular/common/http';

describe('UserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [UserService]
    });
  });

  it('should be created', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));

  it('should get data', inject([UserService], (service: UserService) => {
    service.get().subscribe(data=>{
      expect(data.length).toBeGreaterThan(0);
    });
  }));
  it('should set user', inject([UserService], (service: UserService) => {
    const expectedUser = {firstName:"Betim",lastName:"S"};
    service.selectedUser = expectedUser;
    expect(service.selectedUser).toEqual(expectedUser);
  }));
});
