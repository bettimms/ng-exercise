import {inject, TestBed} from '@angular/core/testing';
import {AuthenticationService} from './authentication.service';

describe('AuthenticationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthenticationService]
    });
  });

  it('should be created', inject([AuthenticationService], (service: AuthenticationService) => {
    expect(service).toBeTruthy();
  }));

  it('should change status', inject([AuthenticationService], (service: AuthenticationService) => {
    //default service.loginStatus is false
    const expectedStatus = true;
    service.onLogin(expectedStatus);
    service.loginStatus.subscribe(s => {
      const status = s;
      expect(status).toEqual(expectedStatus);
    });
  }));
});
