import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../models/user.model';
import 'rxjs/add/operator/map';

//For the sake of optimization get only properties we're interested in
const usersUrl = 'https://randomuser.me/api/?results=20&inc=id,name,email,phone,picture';

@Injectable()
export class UserService {
  users: Observable<Array<User>>;
  selectedUser;

  constructor(private http: HttpClient) {
  }

  get(): Observable<Array<User>> {
    return this.http.get(usersUrl)
      .map(response =>
        this.users = response['results'].map(u => {
          //I wanted to keep using user.model therefore I didnt change original property names [firstName,lastName]
          //Just added new fields
          return {
            id: u.id.value,
            firstName: u.name.first,
            lastName: u.name.last,
            email: u.email,
            phone: u.phone,
            picture: u.picture['large']
          };
        }));
  }
}
