import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ng-e-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {
  @Input() title: string;
  @Input() backUrl: string;

  constructor() {
  }

  ngOnInit() {
  }

}
