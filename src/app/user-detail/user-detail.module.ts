import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserDetailComponent} from './user-detail.component';
import {UserDetailRoutingModule} from './user-detail-routing.module';
import {CoreModule} from '../core/core.module';
import {CapitalizePipe} from '../core/pipes/capitalize.pipe';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    UserDetailRoutingModule
  ],
  declarations: [UserDetailComponent]
})
export class UserDetailModule {
}
