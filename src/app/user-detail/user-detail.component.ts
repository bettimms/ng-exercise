import {Component, OnInit} from '@angular/core';
import {UserService} from '../core/services/user.service';
import {User} from '../core/models/user.model';

@Component({
  selector: 'ng-e-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
  user: User;
  propertyColorClass:string = "text-primary"
  constructor(private userService: UserService) {
  }

  ngOnInit() {
    // this.user = this.userService.selectedUser;//1st approach: Using service, in case of refresh the user is lost
    this.user = JSON.parse(localStorage.getItem('user')) as User;//2nd approach:User persists even in case of refreshing the page
  }

}
