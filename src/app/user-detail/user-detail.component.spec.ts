import {async, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {UserService} from '../core/services/user.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {UserDetailComponent} from './user-detail.component';
import {PageHeaderComponent} from '../core/components/page-header/page-header.component';

describe('UserDetailComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        PageHeaderComponent,
        UserDetailComponent
      ],
      providers: [UserService]
    }).compileComponents();
  }));
  it('should create the component', (async () => {
    const fixture = TestBed.createComponent(UserDetailComponent);
    const comp = fixture.debugElement.componentInstance;
    expect(comp).toBeTruthy();
  }));
  it(`should have an image element`, async(() => {
    const fixture = TestBed.createComponent(UserDetailComponent);
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('img').classList).toContain('card-img-top');
  }));
});
