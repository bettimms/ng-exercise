import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {UserDetailComponent} from './user-detail.component';

export const routes: Routes = [
  {
    path: ':id',
    component: UserDetailComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class UserDetailRoutingModule {

}
